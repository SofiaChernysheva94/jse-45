package ru.t1.chernysheva.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    Iterable<AbstractCommand> getCommandByArgument();

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

}
